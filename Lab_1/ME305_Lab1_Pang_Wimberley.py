'''@file        ME305_Lab1_Pang_Wimberley.py
   @brief       this program changes the light pattern on a LED upon button press
   @details     the program will cycle from start to state 0 to state 1
                upon button B1 being pressed down the finite state machine
                will cycle to state 2, 3, 4, then back to 2, until terminated
   @author      Philip Pang
   @author      Matthew Wimberley
   @date        October 4, 2021
'''   

import utime
import pyb
import math

'''@brief       defines LED that lights up and button to be pressed'''

flg = 0
LD2 = pyb.Pin(pyb.Pin.cpu.A5)
B1 = pyb.Pin(pyb.Pin.cpu.C13) 


def onButtonPressFCN(IRQ_src):
    '''@brief   states flag variable
       @details when flg is 0, the button was pressed
       @param   irq source is button press'''
       
    global flg
    flg = 1
    
def update_timer(startTime):
    '''@brief   function starts timer for each cycle
       @details returns the length of time cycle for each state
       @param   timer value'''
       
    return utime.ticks_diff(utime.ticks_ms(), startTime)
    
def update_sqw(t):
    '''@brief   sets up brightness pattern for square wave
       @param   the time t
       @return  returns number between 0-100 representing LED brightness'''
       
    return 100 * ((t % 1) < 0.5)
    
def update_stw(t):
    '''@brief   sets up brightness pattern for sawtooth wave
       @param   the time t
       @return  returns number between 0-100, 
                goes 0 to 100 linearly then back to 0'''
                
    return 100 * (t % 1)
    
def update_sw(t):
    '''@brief   sets up brightness pattern for sine wave
       @param   the time t
       @return  returns number 0-100, finds sine of number then multiplied x100'''
       
    return 100 * (0.5 * math.sin(math.pi * t / 5) + 0.5)
    

if __name__ == '__main__':

    # the state the Finite State Machine is about to run
    # all functions should be defined before the if __name__ block
    
    state = 0
    ButtonInt = pyb.ExtInt(B1, mode=pyb.ExtInt.IRQ_FALLING,
                           pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)
    '''@brief   defines state variable, timers, and button press
       @details   state always starts at 0, timers use utime embedded in python'''
    
    startTime = utime.ticks_ms()
    runningTime = utime.ticks_ms()
    LEDtimer = pyb.Timer(2, freq = 20000)
    brightness = LEDtimer.channel(1, pyb.Timer.PWM, pin=LD2)
    
    
    while(True):
        try:  # If not keyboard interrupt run FSM
            if(state == 0):
                '''@brief gives user instructions'''
                
                # runs state 0 code
                print('Press the blue button labeled \'B1\' on the Nucleo'
                      'to cycle through square, sine, and sawtooth LED patterns')
                state = 1  # transition to state 1

            elif(state == 1):
                # runs state 1 code
                '''@brief   initiates square wave pattern in state 2'''
                
                if flg == 1:
                    flg = 0
                    state = 2
                    startTime = utime.ticks_ms()
                    print('Square wave pattern selected')

            elif(state == 2):
                # runs state 2 code
                '''@brief   square wave continues until button press to state 3
                   @details timer is restarted and updates to sq wave func'''
                
                elapsed = update_timer(startTime)/1000
                bright = update_sqw(elapsed)
                brightness.pulse_width_percent(bright)
                
                if flg == 1:
                    flg = 0
                    state = 3
                    startTime = utime.ticks_ms()
                    print('Sawtooth pattern selected')
                
                
            elif(state == 3):
                # runs state 3 code
                # sawtooth pattern executes here
                '''@brief   sine wave pattern executes until button press to st 4
                   @details timer is restarted and updates to sin wave func'''
                
                elapsed = update_timer(startTime)/1000
                bright = update_stw(elapsed)
                brightness.pulse_width_percent(bright)
                
                if flg == 1:
                    flg = 0
                    state = 4 # transition to state 4
                    startTime = utime.ticks_ms()
                    print('Sinewave pattern selected')
        

            elif(state == 4):
                # runs state 4 code
                # sinewave patterne exectues here
                '''@brief   sawtooth pattern executes, button press goes to st 2
                   @details timer is restarted and calls updatestw'''
                
                elapsed = update_timer(startTime)/1000
                bright = update_sw(elapsed)
                brightness.pulse_width_percent(bright)
               
                if flg == 1:
                    flg = 0
                    state = 2  # transition to state 2\
                    startTime = utime.ticks_ms()
                    print('Square wave pattern selected')

        except KeyboardInterrupt:  # If keyboard interrupt, exit loop
            break

    print('Program terminating')
