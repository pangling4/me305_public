''' @file           Lab_0.py
    @brief          This file provides an interface that computes the Nth fibonacci number as designated by the user.
    @author         Philip Pang
    @date           Sept 21, 2021
'''

def fib(idx):
    '''
    @brief          Takes care of base cases and calls bottom up calculation method.
    @details        Given an index, fib() returns 0th and 1st terms of fibonacci sequence if requested. For all other
                    indices, the compute_fib() method is called.
    @param idx      A valid index of requested fibonacci number.
    @return         Returns fibonacci number of the requested index.
    '''

    # Base case of idx = 0
    if idx == 0:
        return 0
    # Base case of idx = 1
    if idx == 1:
        return 1
    # If neither base cases are requested, compute_fib is called
    else:
        return compute_fib(idx, 0, 1, 0)
    
def compute_fib(idx, num1, num2, i):
    '''
    @brief          Uses bottom-up calculation method to compute 2nd, 3rd, 4th... fibonacci numbers
    @details        For the sake of speed, the bottom-up method is used so that computations are not repeated
                    unnecessarily. It calculates the previous fib number and then adds the one before that to calculate
                    the next fib number
    @param idx      The index of requested fibonacci number.
    @param num1     The fib # before the previous fib #
    @param num2     The previous fib #
    @param i        Index to keep track of which fibonacci number was just calculated
    @return         Returns fibonacci number of the requested index.
    '''

    # Base case when compute_fib has reached the required fibonacci number in the sequence
    if i==idx-1:
        return num2
    # If base case has not been reached, the second number is stored as the first and a new second number is computed
    # as the sum of the current numbers
    num = num1 + num2
    num1 = num2
    num2 = num
    # Increment the tracking index
    i+=1
    # Call function recursively to continue computation
    return compute_fib(idx, num1, num2, i)
    
# Testing Code
#
# The following code provides an interface to test fib() as a console application. It rejects and gives feedback for
# invalid input from the user. Until "exit" is entered, the program will continue to prompt the user to enter an index
# to find the corresponding fibonacci number.
if __name__ == '__main__':
    while(1):
        idx = -1
        user = input('\nEnter a positive integer (or type \'exit\' to exit program): ')
        
        try: 
            if user == 'exit':
                break
        except:
            print('A non-integer cannot be accepted')  
        
        try:
            idx = int(user)
            if idx < 0 :
                print('A negative integer cannot be accepted')
                idx = -1
        except:
            print('A non-integer cannot be accepted')
            idx = -1
        
        if (idx != -1): 
            print('Fibonacci number at '
                  'index {:} is {:}.'.format(idx, fib(idx)))